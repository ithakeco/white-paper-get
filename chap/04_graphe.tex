\chapter{Graphe de l'écologie territoriale}

\begin{figure}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{res/graph-ex.png}
        \caption{Exemple de graphe composé de sommets (points) et d'arêtes (segments) -- Graphe généré avec le modèle mathématique de "petit monde" fréquemment utilisé pour modéliser les réseaux sociaux.\label{png-graph}}
    \end{center}
\end{figure}

\section{Définition}
Un graphe est une structure mathématique qui modélise des relations entre plusieurs éléments. Il est composé d'un ensemble de sommets et d'un ensemble d'arêtes qui représentent respectivement les éléments à modéliser et les relations entre ces éléments (voir la figure~\ref{png-graph}). Un graphe peut, par exemple, représenter un réseau routier, dans lequel les sommets représentent les intersections et les arêtes représentent les routes, ou encore un réseau social, dans lequel les sommets représentent les personnes et les arêtes représentent les relations entre les personnes. Les graphes peuvent être utilisés pour représenter tout type de relations, et de nombreux algorithmes ont été développés afin d'analyser les systèmes complexes qu'ils représentent. 

Le Graphe de l'\'Ecologie Territoriale (GET) combine au sein d'un même modèle de donnés les acteurs d'un territoire (habitant, association, entreprise, ...), les actions qu'ils peuvent entreprendre (atelier, événement, groupement d'achat, ...) et les ressources qu'ils ont à disposition (matériaux, infrastructure, outils, ...). Les relations entre ces éléments peuvent être de différentes natures. Les arêtes du graphe peuvent ainsi modéliser les interactions directes ou indirectes qui ont eu lieu entre les utilisateurs d'une plate-forme numérique (lien sur un réseau social, transaction, entraide, ...), mais également représenter la quantité d'équivalent carbone émis lorsqu'une action est effectuée, ou le coût économique nécessaire à sa réalisation.

Les graphes dans lesquels les sommets et les arêtes peuvent avoir des types différents sont appelés graphes hétérogènes. Ce sont des objets plus complexes que les graphes homogènes, mais ils permettent de modéliser des systèmes plus riches et plus complexes. Le GET appartient à cette catégorie de graphe.

\section{Méthode de collecte}
La spécification et la modélisation du GET mobilisent de  nombreuses connaissances issues de l'aménagement du territoire, des associations locales, des filières d'approvisionnement, du traitement des déchets, etc. Cette initiative se traduit donc, pour un territoire donné, par la mobilisation d'une multiplicité d'acteurs et d'experts locaux à travers une démarche participative. Impliquer les acteurs dans une approche de co-construction permet d'assurer une meilleure adaptation au tissu local, et donc une plus grande richesse du graphe. Comme pour tout modèle de données, les étapes de récupération, de structuration, de traitement et de protection des données sont cruciales. Les données peuvent être collectées par différentes méthodes, telles que des enquêtes, des interviews d'experts et des observations de terrains. 

Néanmoins, le passage à l'échelle de cette démarche est rendu possible par l'opération sur le territoire d'une stratégie de plate-forme numérique permettant de connecter l'ensemble des acteurs de l'écosystème et de faciliter la collecte des données. La massification des informations contenue dans le graphe repose donc sur la création d'une plate-forme pour donner aux acteurs les moyens de construire en commun et d'activer la transformation écologique, de mettre en œuvre les effets de réseaux au sein de l'écosystème des habitants, des associations et des professionnels pour agir de façon concrète sur le terrain.

L'objectif est de proposer une infrastructure numérique permettant aux acteurs territoriaux de conduire une stratégie de plateforme et ainsi mobiliser une transformation fondamentale de l'écologie territoriale. Trois leviers d'adhésion ont été identifiés pour la captation des données. Concrètement, cette plate-forme doit permettre pour les territoires et leurs acteurs de :
\begin{itemize}
    \item \textbf{Faciliter les accès} numériques (démarches en ligne, réservations, demandes d'aide,~...) et physiques (locaux associatifs, médiathèques, déchèteries, ...) à l'aide d'un identifiant unique territorial généralisable au plus grand nombre de services et d'équipements du territoire. Permettre aux acteurs du territoire un accès et une gestion de ces accès polyvalents, simples, efficaces et sûrs.
    \item \textbf{Intermédier les flux} et échanges financiers, par le biais de Monnaie Locales Complémentaires ou autres devises vertueuses pour permettre aux usagers d'utiliser cette devise de façon simple et d'en gérer la comptabilité de façon sûre et efficace.
    \item \textbf{Faciliter la contribution} en apportant aux usagers confiance, transparence et contrôle sur leurs données et leurs usages. Mettre en œuvre des techniques de profils numériques structurés et sécurisés, qui faciliteront l'apprentissage et l'opération d'algorithmes sur la base de chaînes d'activités quotidiennes riches.
\end{itemize}

Le profil territorial unique permet de stocker et de partager les données des utilisateurs de manière décentralisée, en maintenant leur capacité de contrôle sur leurs données. Sur la base des données de profils, l'opérateur de la plate-forme mettra en œuvre des algorithmes (analyse, ciblage, apprentissage automatique, modélisation, ...) pour Orienter, Suggérer et Inciter les évolutions individuelles, collectives et systémiques. On procédera au travers de recommandations ciblées (adaptées, abordables, accessibles, compatibles, lisibles), et idéalement inscrite dans des parcours de vie qui permettent un effet « domino ». Les données nécessaires pour la construction d'un graphe de l'écologie territoriale peuvent être séparées en trois catégories qui correspondent trois types de sommets dans le graphe :

\subsubsection*{Données sur les Acteurs}
Il s'agit d'identifier les différents acteurs d'un territoire, tels que les entreprises, les associations, les particuliers ou les institutions publiques. Pour chaque acteur, il est utile de collecter des informations telles que son nom, son adresse, son activité, ses ressources et ses habitudes. Il est également nécessaire de garder une trace des interactions directes ou indirectes qui ont lieu entre les acteurs, telles que les échanges de biens, de services, d'informations, etc.

\subsubsection*{Données sur les Actions}
Il s'agit d'identifier les différentes actions que les acteurs peuvent entreprendre pour participer aux initiatives locales et réduire leur empreinte. Pour chaque action, il est nécessaire de collecter des informations telles que son nom, ses parties prenantes, sa description, ses impacts environnementaux, économiques et sociaux...

\subsubsection*{Données sur les Ressources}
Il s'agit d'identifier les différentes ressources dont les acteurs disposent, telles que la terre, l'eau, l'énergie, les matières premières ou des équipements spécifiques. Pour chaque ressource, il est nécessaire de collecter des informations telles que sa quantité, sa localisation, sa valeur, son impact environnemental équivalent CO2...

\begin{figure}
    \begin{center}
        \includegraphics[width=0.6\textwidth]{res/get.png}
        %\caption*{Exemple de graphe composé de sommets (cercles) et d'arêtes (segments)\label{png-graph}}
        \includegraphics[width=0.6\textwidth]{res/get-sort.png}
        \caption{Deux représentations d'un même graphe hétérogène avec les actions en vert (ensemble $P$), les acteurs en bleu (ensemble $A$) et les ressources en orange (ensemble $R$)\label{get-ex}}
    \end{center}
\end{figure}

\section{Usages du GET}
Le graphe de l'écologie territoriale est un outil en plein développement, avec de nombreuses possibilités d'utilisation. Il a l'ambition et le potentiel de devenir le référentiel indispensable pour la transition écologique. Des travaux de recherche sont en cours pour révéler ce potentiel, notamment sur l'utilisation des réseaux de neurones en graphes ou GNNs (\emph{Graph Neural Networks}) couplée avec les technologies de l'Informatique Confidentielle (\emph{Confidential computing}). Les GNNs constituent un prolongement des réseaux de neurones capables de manipuler des données issues d'un graphe complexe. Ils produisent des résultats prometteurs et peuvent permettre : de classifier les acteurs selon leurs données de profil (i.e. classification automatique) ; de leur recommander des actions écologiques et sociales (i.e. système de recommandation) et de prédire si des acteurs sont sujets à agir ensemble (i.e. prédiction de lien). Dans ce contexte, l'informatique confidentielle viserait à assurer la confidentialité et l'intégrité des données sensibles contenues dans le graphe.

\subsubsection*{Orienter}
Le graphe de l'écologie territoriale peut être utilisé pour mettre en évidence les zones à faible potentiel écologique (cf 4.4). Ces zones sont caractérisées par l'absence d'infrastructure, de ressources, et d'acteurs engagés dans la transition écologique. Cette information peut être utile pour mesurer les efforts de développement  durable à appliquer sur les différentes zones géographiques, en identifiant les ressources clés et les acteurs qui en disposent, afin de les mutualiser pour maximiser l'impact lié à leur utilisation.

Il peut montrer les liens entre les actions écologiques et les synergies qui existent entre elles. Il devient dès lors possible de proposer des chaînes d'actions vertueuses qui fonctionnent efficacement dans un territoire donné et d'étudier dans quelle mesure cette configuration est transférable à d'autres zones.

\subsubsection*{Suggérer}
Le graphe de l'écologie territoriale peut être utilisé pour quantifier les impacts environnementaux, économiques et sociaux des actions effectuées. La structure de graphe permet de calculer dans le détail l'ensemble des étapes et des ressources nécessaires à la réalisation de chaque action, et peut ainsi révéler les “\emph{bottlenecks} carbone”, qui font qu'un parcours de vie donné soit plus gourmand qu'un autre, par exemple en mesurant l'impact carbone d'une chaîne d'approvisionnement. Ce calcul permet de suggérer des alternatives (recherche de plus courts chemins dans le graphe de consommation carbone) qui réduirait le coût carbone.  Une entreprise responsable pourrait par exemple utiliser le graphe pour identifier les fournisseurs et les clients qui ont un impact environnemental important, et prendre les mesures nécessaires pour un changement vertueux

\subsubsection*{Inciter}
Le graphe permet également de renforcer les liens entre les acteurs d'un territoire, en tissant des liens entre des individus et des groupes qui n'auraient pas eu l'occasion d'interagir précédemment. On évite ainsi l'isolement de certains acteurs, et de nouvelles synergies peuvent émerger, se traduisant par des économies d'échelle, des effets de levier, et une augmentation de la résilience du réseau à la disparition de certains acteurs. Par exemple, le graphe peut être utilisé pour identifier des associations qui travaillent sur des sujets similaires, mais qui ne sont pas directement connectées entre elles. Il est possible d'organiser des événements ou des rencontres pour favoriser les échanges entre ces associations, mettre en commun leurs ressources et élargir leur public.

Il est également possible d'analyser dans le graphe les relations entre les acteurs d'un territoire et de catégoriser (en fonction de différents critères, tels que le type d'acteur, le secteur d'activité, la taille de l'acteur, ou encore la localisation) les différents groupes d'acteurs d'un territoire grâce à des algorithmes de classification automatique (\emph{clustering}).  La définition de ces groupes et l'identification des acteurs centraux du graphe (influenceurs) permet d'adapter le message et de cibler précisément les actions de communication tout en évitant d'envoyer des recommandations non pertinentes certain·e·s utilisateur·rice·s.

\section{Potentiel écologique}
Dans le cadre formel du graphe de l'écologie territoriale, la notion de potentialité écologique est une nouvelle métrique qui fait référence à l'ensemble des leviers et des opportunités que les acteurs ont à leur disposition dans leur environnement pour faciliter la transition écologique et sociale. Elle met en avant le potentiel de facilitation et les ressources disponibles localement pour favoriser les évolutions de la société et des modes de vie. La potentialité écologique repose sur l'idée que chaque acteur a la capacité de contribuer positivement à l'environnement en prenant des mesures concrètes à son échelle. En adoptant ces actions et en encourageant les autres à faire de même, il est possible de réduire collectivement l'empreinte d'un territoire.

L'analyse du potentiel écologique se découpe en cinq étapes principales:
\begin{itemize}
    \item Collecte de données hétérogènes et d'indicateurs territoriaux pour la création d'un référentiel de données du territoire
    \item Identification des liens entre acteurs du territoire sur la base du travail d'enquête de terrain et des traces d'activité sur les outils numérique
    \item Calcul du vecteur de potentiel par la pondération des liens identifiés selon différents indicateurs correspondant aux cibles de la planification écologique et territoriale
    \item Propagation du potentiel de chaque lien sur les liens situés dans le voisinage.  La propagation va régulariser les potentiels de manière à ce que le potentiel final d'un lien sera un compromis entre son score individuel et le score de ses voisins.
    \item Détection de motifs et identification de cibles clés du graphe de l'écologie qui sont les plus influentes en raison de leur potentiel élevé.
\end{itemize}

Exprimer la potentialité écologique en termes d'équivalent carbone dépend de variables telles que les lieux d'activité (et les infrastructures existantes aux alentours), les modes de vie actuels et les ressources des acteurs. Le graphe de l'écologie territoriale nous permet d'estimer le potentiel écologique d'une zone en mettant en lien de nombreux paramètres, voici quelques exemples~:

\subsubsection*{Transport durable}
Estimer la distance parcourue par un acteur en voiture chaque jour et la multiplier par le facteur d'émission moyen pour les véhicules à essence. Ensuite, estimer la distance que l'individu pourrait parcourir à vélo, à pied ou en utilisant les transports en commun accessibles près de son lieu de d'activité et calculer les émissions associées.

\subsubsection*{Consommation responsable}
Identifiez les aliments consommés régulièrement et estimer la part de l'alimentation de l'individu qui est composée de produits ayant un impact important de par leur mode de production ou de transport. Faire la différence avec une alimentation où l'individu remplacerait certains produits par des alternatives locales et bio disponibles autour de chez lui, ou encore en cultivant un jardin partagé en utilisant des méthodes respectueuses de l'environnement.

\subsubsection*{Gestion des déchets}
Identifiez la quantité de déchets qu'un acteur génère en moyenne par jour et estimer combien de ces déchets pourraient être recyclés ou compostés. Consulter les taux de réduction d'émissions généralement associés à la logistique nécessaire au recyclage et au compostage de ces déchets.

\subsubsection*{Sensibilisation et engagement}
La possibilité d'un acteur de partager ses connaissances et de sensibiliser son réseau aux enjeux environnementaux en participant à des actions collectives, des associations locales ou des initiatives de protection de l'environnement pourrait également être pris en compte

Les réductions au niveau individuel peuvent être estimées avec précision grâce à une population synthétique \citep{horl2021synthetic}. Il devient alors par exemple possible de sommer les potentiels individuels pour obtenir des “\emph{coldspots}” (avec le potentiel individuel qui rayonnant de manière inversement proportionnel à la distance)  du territoire où il serait prioritaire  de concentrer nos efforts.

\subsubsection*{Optimiser l'effort}
%Le potentiel écologique repose sur l'idée que chaque acteur est en capacité de contribuer positivement à la transition en prenant des mesures concrètes à son échelle. En modifiant sa manière d'agir et d'interagir au quotidien, en participant à des actions collectives et en encourageant les autres à faire de même, il est possible de réduire collectivement l'empreinte carbone d'un territoire.

Les actions à impact sont d'ores et déjà identifiées par les experts et les pouvoirs publics. L'idée ici est également de proposer une mesure objective permettant de surmonter les difficultés à percevoir les liens, les conditions, la portée et les impacts sur le territoire. L'un des atouts du graphe de l'écologie territoriale est de permettre d'estimer localement les potentiels mais également d'utiliser la notion de voisinage dans le réseau pour propager les potentialités et ainsi construire une évaluation systémique du potentiel écologique.

La complexité et l'hétérogénéité du graphe de l'écologie territoriale peuvent donner à certains sommets un rôle prépondérant. Certains sommets sont ainsi considérés comme plus importants car ils impactent significativement l'ensemble du graphe. L'identification des sommets importants dans le graphe complexe est une tâche importante pour guider et opérer la transition écologique et sociale.

%\section{Intelligence Artificielle}

\section{Protection des données}		
La sécurisation et la gouvernance des données sensibles est un enjeu majeur pour la conception du GET. Il est notamment important que les acteurs redeviennent propriétaires de leurs données et que des protocoles soient mis en place pour permettre le stockage et l'usage des données tout en garantissant la confidentialité et la sécurité. Certaines technologies permettant l'usage des données doivent être mises en oeuvre pour opérer le graphe de l'écologie de manière fiable et éthique :
\begin{itemize}
    \item Chiffrement Homomorphe : Permet de traiter des données chiffrées sans avoir besoin de les déchiffrer au préalable, elles restent protégées tout au long du traitement
    \item Preuve à Divulgation Nulle : Prouve qu'une proposition est vraie sans toutefois révéler d'autres informations que la véracité de la proposition
    \item Apprentissage Fédéré : Permet de réduire les risques de confidentialité et de vie privée, car les données les plus sensibles restent sur les appareils locaux
    \item Confidentialité Différentielle : Permet l'analyse algorithmique des données personnelles sans en révéler les informations en maintenant leur potentiel d'exploitation
\end{itemize}

\section{Démonstrateur}
La carte Eco est le démonstrateur «~grandeur nature~» qui ambitionne de démontrer le potentiel d'une plateforme numérique permettant de construire le Graphe de l'\'Ecologie Territoriale des acteurs du territoire de Plaine Commune. Cette carte est un pass multi-service numérique pour le territoire de Plaine Commune Grand Paris dans le nord de l'\^Ile-de-France. Le but de l'outil est de réconcilier économie et écologie en accompagnant les acteurs du territoire dans la transition écologique.

Plaine Commune est un Établissement Public Territorial (EPT) qui regroupe 9 villes au nord de Paris (450~000 habitants). Elles sont fédérées autour d’un projet commun, sur un espace qui connaît des mutations inédites en région parisienne. Territoire stratégique en \^Ile-de-France, Plaine Commune poursuit sa transformation augmentée par l’accueil des Jeux olympiques et paralympiques en 2024.

En 2023, l'EPT a cocrée l'association Eco pour se doter d'une capacité d'entrainement des acteurs économiques et des habitants dans la transformation écologique du territoire et des modes de vie, et d'une capacité de valorisation de cet engagement. Ce projet, développé dans le cadre du Fonds d'innovation de la Solidéo~\footnote{Société de livraison des ouvrages olympiques}, repose sur le déploiement d'une monnaie locale écologique, d'une application numérique, et d'une méthode de valorisation des pratiques et des achats écologiques des utilisateurs de la monnaie.

C'est dans ce contexte que l'entreprise Ithake intervient : développer la plate-forme opérée sur le territoire de Plaine Commune par l’association Eco. Cette application est un rouage clé d’une infrastructure numérique territoriale qui devra permettre à terme d’évaluer les impacts des individus et services qui animent l’écologie territoriale, de conduire un suivi de la durabilité du système territorial, et d’influencer les transformations individuelles, collectives et systémiques permettant d’atteindre la durabilité.





