\chapter{Cadre scientifique}

Les systèmes complexes représentent des ensembles constitués d'un grand nombre d'entités en interaction~\citep{huraux2015simulation}. Ces systèmes sont caractérisés par des propriétés émergentes qui n'existent qu'au niveau macroscopique et qui ne peuvent être observées qu'au niveau de ses constituants (i.e. le niveau microscopique). Exprimé souvent par l'expression populaire « le tout est plus que la somme des parties », ce concept d'émergence est caractéristique de nombreux phénomènes économiques, sociaux et environnementaux.

\section{Réseaux complexes}
Apparue plus récemment, la théorie des réseaux complexes~\citep{li2020projet} permet de représenter les systèmes complexes de la vie réelle (villes~\citep{ding2019complex}, réseaux sociaux~\citep{daud2020applications}, écosystèmes~\citep{gonzalez2023evolution}, ...) sous forme de modèles mathématiques par des structures de réseau. L'idée ici est d'abstraire les entités d'un système sous forme de nœuds et les relations entre les entités sous forme d'arêtes. Favorisée par ces avancées scientifiques, la compréhension des dynamiques qui régissent les phénomènes humains, tels que la consommation et les relations sociales, est devenue un enjeu stratégique majeur pour les géants du numérique (i.e. GAFAM). Le succès sans précédent des plate-formes en ligne leur donne accès à des «~Social Graphs~», des réseaux de connexions et de relations entre les utilisateurs et les objets les plus massifs à ce jour~\citep{noy2019industry}.

Dans ce contexte, les systèmes de recommandation et d'évaluation basés sur les réseaux complexes sont devenus les moyens privilégiés de mise en œuvre des stratégies marketing.

\section{Systèmes de recommandation}
Un système de recommandation est un programme informatique capable de suggérer des éléments d'intérêt à une personne sur la base d'un algorithme qui apprend à partir de données~\citep{kembellec2014moteurs}. Les systèmes de recommandation sont utilisés dans de nombreux domaines, notamment les sites de commerce en ligne, les services de vidéo à la demande, la musique en ligne, les actualités, les sites de rencontre, ... Un exemple emblématique est celui d'Amazon, qui personnalise ses pages web pour chaque utilisateur en fonction de son profil et surtout des produits et marques sponsorisés. Pour fonctionner, les systèmes de recommandation utilisent principalement l'évaluation des utilisateurs et leur similarité pour proposer des produits/services personnalisés.

Contrairement à la majorité des travaux antérieurs sur les systèmes de recommandation, qui se sont concentrées sur l'amélioration de la précision des recommandations en modélisant mieux les préférences de l'utilisateur pour présenter les éléments préférés individuellement, nous nous concentrons sur les systèmes de recommandation en tant qu'outil numérique pour favoriser massivement la transition écologique à l'échelle individuelle mais surtout collective.
Les systèmes de recommandation peuvent être catégorisés en trois types principaux : le filtrage basé sur le contenu~(1), le filtrage collaboratif~(2) et le filtrage hybride~(3):

\subsubsection*{(1) Filtrage basé sur le contenu}
Ce type d'algorithme utilise la similitude entre les objets pour en recommander des semblables à ceux que l'utilisateur apprécie. En fonction des objets que le client a sélectionnés/acceptés/appréciés/… dans le passé, ces systèmes de recommandation sont capables de présenter aux utilisateurs des objets susceptibles de les intéresser. Ici, pas besoin d'évaluation ou d'avis de la part des utilisateurs, mais il est nécessaire de définir une mesure de similarité entre les objets à recommander et ceux avec lesquels l'utilisateur a déjà interagi.

\emph{Exemple : Si l'utilisateur “A” accepte deux actions sur l'auto réparation des vélos, le système peut recommander d'autres actions sur le thème de la pratique du vélo.}

\subsubsection*{(2) Filtrage collaboratif}
Le filtrage collaboratif est une technique qui permet de filtrer les objets qu'un utilisateur pourrait apprécier/accepter sur la base des réactions d'utilisateurs similaires. Cette technique consiste à filtrer parmi tous les utilisateurs celles et ceux qui sont similaires pour examiner les objets qu'ils aiment et les combiner pour au final créer une liste de suggestions classées. Il existe de nombreux algorithmes permettant de déterminer quels utilisateurs sont similaires et de combiner leurs choix pour créer une liste de recommandations.

\emph{Exemple : Si l'utilisateur “A” est semblable à l'utilisateur “B” et que l'utilisateur “B” a apprécié une action, le système peut recommander cette même action à l'utilisateur “A”.}

\subsubsection*{(3) Filtrage hybride}
Dans la pratique, de nombreux systèmes de recommandation sont dits “hybrides” dans la mesure où ils combinent les atouts de différentes technologies mentionnées précédemment pour en améliorer les performances. Il est notamment possible de calculer des recommandations basées sur le contenu et d'utiliser le filtrage collaboratif de manière indépendante et en les combinant ensuite.

\subsubsection*{}
Tous ces algorithmes de recommandation reposent sur le concept de relation (utilisateur-utilisateur, objet-objet ou objet-utilisateur). Ces relations forment un réseau logique de nœuds, pouvant être considéré comme un réseau complexe.  Sur cette base, les principes des réseaux complexes peuvent être utilisés pour la construction des systèmes de recommandation~\citep{arthur975complex}.

\section{Systèmes d'évaluation}
La réduction carbone d'une collectivité nécessite la compréhension des impacts multidimensionnels environnementaux, économiques et sociaux en utilisant un cadre holistique d'évaluation et de modélisation de la durabilité. Un graphe mettant en relation les acteurs d'un territoire permet de mesurer l'impact carbone en identifiant les flux de matière et d'énergie entre ces acteurs.

Pour chaque flux, il est possible de calculer les émissions carbones associées en fonction des facteurs d'émission de chaque type de matière ou d'énergie.  Par exemple, les émissions de GES associées au transport des produits agricoles vers les transformateurs seront fonction de la distance parcourue, du mode de transport utilisé et de la quantité de produits transportés. 

En additionnant les émissions de GES associées à tous les flux, il est possible de calculer l'impact carbone total de la chaîne d'approvisionnement. L'utilisation d'un graphe permet de tenir compte de la complexité des interactions entre les différents acteurs d'un territoire. Cela permet d'identifier des opportunités d'amélioration de l'efficacité énergétique et de réduction des émissions de GES.

Par exemple~\cite{abdella2020sustainability} quantifient les impacts de durabilité des catégories de consommation alimentaire à l'aide de tables d'entrées-sorties à haute résolution sectorielle de l'économie américaine. Ils proposent un cadre de modélisation permettant d'évaluer la durabilité du cycle de vie de l'input-output économique en utilisant des  techniques d'apprentissage machine supervisées. Cela leur permet d'identifier les goulots d'étranglements responsables de la majorité des émissions, et de suggérer des solutions spécifiques afin de réduire ces dernières.

\cite{taghikhah2021integrated}~proposent quand à eux un modèle qui prend en compte les facteurs opérationnels et comportementaux des fournisseurs de produits alimentaires et des consommateurs, et permet d'analyser l'impact de différentes politiques et stratégies sur la production et la consommation de produits alimentaires durables. L'étude porte également sur un nouveau concept de chaîne d'approvisionnement agroalimentaire étendue, qui prend en compte les interactions entre les différents acteurs de la chaîne, de la production à la consommation. 

Ce type de modèles permet de simuler le comportement adaptatif des différents acteurs face à des changements dans les préférences des consommateurs ou dans les conditions de production afin de comprendre les conditions qui encouragent la production et la consommation de produits plus respectueuse de l'environnement, en prenant en en compte les aspects économiques, environnementaux et sociaux de la production. 

La mise en graphe et l'analyse input-output peuvent servir d'outils d'aide à la décision pour les décideurs qui souhaitent développer des politiques et des stratégies pour promouvoir des “parcours de vie” plus durables. Ces outils permettent d'identifier les interventions qui sont les plus susceptibles d'être efficaces et d'évaluer l'impact de ces interventions sur la production et la consommation de produits alimentaires durables.

\begin{figure*}
    \begin{center}
        \includegraphics[width=\textwidth]{res/input-output.png}
        \caption{Exemple d'analyse Inputs-Outputs pour un projet de cuisine participative}
        \label{IO}
    \end{center}
\end{figure*}
